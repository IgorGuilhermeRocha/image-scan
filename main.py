import optparse
import os
import requests

from image_scan import ImageScan








def main():
    analyzer = optparse.OptionParser('use main.py '+\
      '-U <URL>')
    analyzer.add_option('-U', dest='url', type='string',\
      help='especifique a url desejada.')

    (opcoes, args) = analyzer.parse_args()

    url = opcoes.url

    if (url == None):
        print (analyzer.usage)
        exit(0)

    image_scan = ImageScan()
    image_scan.search_images(url)
    image_scan.scan_images_with_gps()


if __name__ == '__main__':
    main()