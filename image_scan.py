import os
from bs4 import BeautifulSoup
import bs4
from PIL import Image
from PIL.ExifTags import TAGS

import requests


class ImageScan:
    BASIC_FOLDER_DIRECTORY = 'images'
    GPS_DIRECTORY = 'images_with_gps'

    @staticmethod
    def search_images(url: str) -> None:
        if not os.path.exists(ImageScan.BASIC_FOLDER_DIRECTORY):
            os.makedirs(ImageScan.BASIC_FOLDER_DIRECTORY)
        img_tags = ImageScan._html_parser(url)
        ImageScan._save_images(img_tags)

    @staticmethod
    def _html_parser(url: str) -> bs4.element.ResultSet:
        response = requests.get(url)
        html_content = response.content
        soup = BeautifulSoup(html_content, "html.parser")
        img_tags = soup.find_all("img")
        return img_tags

    @staticmethod
    def _save_images(img_tags: bs4.element.ResultSet):
        for img_tag in img_tags:
            try:
                img_url = img_tag["src"]
                img_name: str = img_url.split("/")[-1]
                if not img_name.endswith('.png'): img_name += '.png' 
                img_path = os.path.join(ImageScan.BASIC_FOLDER_DIRECTORY, img_name)
                with open(img_path, "wb") as img_file:
                    try:
                        img_file.write(requests.get(img_url).content)
                    except Exception:
                        continue
            except KeyError:
                pass

    @staticmethod
    def scan_images_with_gps():
        for file_name in os.listdir(ImageScan.BASIC_FOLDER_DIRECTORY):
            file_path = os.path.join(ImageScan.BASIC_FOLDER_DIRECTORY, file_name)
            if os.path.isfile(file_path):
                try:
                    exif_data = {}
                    img = Image.open(file_path)
                    info = img._getexif()
                    if info:
                        for (tag, value) in info.items():
                            decoded = TAGS.get(tag, tag)
                            exif_data[decoded] = value
                        exif_gps = exif_data['GPSInfo']
                        print(exif_data)
                        if exif_gps:   
                            with open(file_path, "wb") as img_file:
                                img_file.write(img_file)
                except Exception as e:
                    print(e)

                
                